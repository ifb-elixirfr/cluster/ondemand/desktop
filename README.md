# Desktop

This app let you start an XFCE desktop on a SLURM cluster
It is inspired by the Open OnDemand integrated Desktop app (see https://osc.github.io/ood-documentation/latest/enable-desktops.html)